import $ from 'jquery';
import Swiper from 'swiper';
import '../../node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min';
import '../../node_modules/paroller.js/dist/jquery.paroller.min';
import '../../node_modules/jquery-mask-plugin/dist/jquery.mask.min';
import '../../node_modules/jquery-validation/dist/jquery.validate.min';
import '../../node_modules/jquery-popup-overlay/jquery.popupoverlay';

$(window).on('load',function() {
  $('.nav').mCustomScrollbar({
    theme: 'minimal',
  });
  $('.faq__tabs_elements').mCustomScrollbar({
    theme: 'minimal',
  });
  $('.faq__tabs_text').mCustomScrollbar({
    theme: 'minimal',
  });
  $('.privacy-policy_text').mCustomScrollbar();

});


$(window).scroll(function() {
  // - - - - SCROOL - - - -
  if($(this).scrollTop() > 70) {
    $('.header').addClass('scroll');
  }
  else {
    $('.header').removeClass('scroll');
  }
});

var swiperTwo = new Swiper('.reviews__slider .swiper-container', {
  slidesPerView: 1,
  spaceBetween: 30,
  loop: true,
  autoHeight: true,
  pagination: {
    el: '.swiper-pagination',
    type: 'fraction',
  },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },

});

$(document).resize(function() {
  swiperTwo.update();
  swiper.update();
  $('.reviews__block_text').each(function(index, elem) {
    swiperTwo.update();
    readMore(elem);
  });
});


function readMore(el) {
  var elem = $(el);
  var fullHeight = elem.innerHeight();
  var maxHeight = 178;
  var moreText = 'Читать весь отзыв';
  var lessText = 'Спрятать';
  var btn = elem.next();

  $(document).resize(function() {
    fullHeight = elem.innerHeight();
    if (parseInt(elem.css('height'), 10) !== fullHeight && parseInt(elem.css('height'), 10) !== maxHeight) {
      elem.css('height', maxHeight).animate({
        height: fullHeight,
      },
      1000, function() {
      });
    }

  });

  elem.css({
    height: maxHeight
  });

  btn.click(function(e) {
    e.preventDefault();
    swiperTwo.update();

    if (parseInt(elem.css('height'), 10) !== fullHeight) {
      elem.css('height', maxHeight).animate({
        height: fullHeight,
      },
      1000, function() {
        elem.addClass('active');
        btn.html(lessText);
        swiperTwo.update();
      });
    }
    else {
      elem.animate({
        height: maxHeight,
      },
      1000, function() {
        elem.css('height', maxHeight);
        elem.removeClass('active');
        btn.html(moreText);
        swiperTwo.update();
      });
    }

  });
}

$('.reviews__block_text').each(function(index, elem) {
  swiperTwo.update();
  readMore(elem);
});



// - - - - FIRST-SCREEN-SWIPER - - - -
var swiper = new Swiper('.first-screen__slider .swiper-container', {
  spaceBetween: 70,
  loop: true,
  pagination: {
    el: '.swiper-pagination',
  },
  autoplay: {
    delay: 5000,
    disableOnInteraction: false,
  },
});


$(document).ready(function() {
  $('.parallax').paroller();
  //--- MODAL ----
  $('.modal').popup({
    transition: 'all 0.3s',
    outline: true, // optional
    focusdelay: 400, // optional
    vertical: 'top', //optional
    // onclose: function() {
    //   $(this).find('label.error').remove();
    // }
  });
  // - - - - ANCHOTN - - - -
  $('.menu a').on('click', function(event) {
    var target = $(this.getAttribute('href'));

    if (target.length) {
      event.preventDefault();
      $('html, body').stop().animate({
        scrollTop: target.offset().top
      }, 1000);
    }
    $('.header').toggleClass('active');
    $('.nav-button').toggleClass('active');
    $('.nav').toggleClass('active');
  });

  function tabsCurrent() {
    var e = $('.faq__tabs_tab').length, a = $('.faq__tabs_tab.active').index() + 1;
    $('.faq__tabs_max').text(e);
    $('.faq__tabs_current').text(a);
  }
  $('#next').click(function() {
    var activeElem =   $('.faq__tabs_tab.active');
    var countElem = $('.faq__tabs_tab').length;
    var activeItem = $('.faq__tabs_item.active');
    if ((activeElem.index()  + 1) <= (countElem - 1)) {
      activeElem.removeClass('active').next().addClass('active');
      activeItem.hide().not(function() {
        activeItem.removeClass('active').next().fadeIn().addClass('active');
      });
      tabsCurrent();
    }
    tabsCurrent();
  });
  $('#prev').click(function() {
    var activeElem =   $('.faq__tabs_tab.active');
    var activeItem = $('.faq__tabs_item.active');
    if (activeElem.index() >= 1 ) {
      activeElem.removeClass('active').prev().addClass('active');
      activeItem.hide().not(function() {
        activeItem.removeClass('active').prev().fadeIn().addClass('active');
      });
      tabsCurrent();
    }
  });
  $('.faq__tabs_item').not(':first').hide();
  $('.faq__tabs_tab').click(function() {
    $('.faq__tabs_tab').removeClass('active').eq($(this).index()).addClass('active');
    $('.faq__tabs_item').removeClass('active').hide().eq($(this).index()).fadeIn().addClass('active');
    tabsCurrent();
  }).eq(0).addClass('active');



  tabsCurrent();
  // - - - - UPDATE-SWIPER - - - -
  swiperTwo.update();
  // - - - - HAMBURGER - - - -
  $('.nav-button').click(function() {
    $('.header').toggleClass('active');
    $('.nav-button').toggleClass('active');
    $('.nav').toggleClass('active');
  });
// --- jQuery Mask + jquery VALIDATION ---
  $('input[type="tel"]').mask('+7 (000) 000-00-00');
  jQuery.validator.addMethod('phoneno', function(phone_number, element) {
    return this.optional(element) || phone_number.match(/\+[0-9]{1}\s\([0-9]{3}\)\s[0-9]{3}-[0-9]{2}-[0-9]{2}/);
  }, 'Введите Ваш телефон');
  $('.form').each(function(index, el) {
    $(el).addClass('form-' + index);

    $('.form-' + index).validate({
      rules: {
        name: 'required',
        agree: 'required',
        tel: {
          required: true,
          phoneno: true
        }
      },
      messages: {
        name: 'Неправильно введенное имя',
        tel: 'Неправильно введен телефон',
        agree: 'Нужно соглашение на обработку данных',
      },
      submitHandler: function(form) {
        var t = $('.form-' + index).serialize();
        console.log(t);
        ajaxSend('.form-' + index, t);
      }
    });
  });
  function ajaxSend(formName, data) {
    jQuery.ajax({
      type: 'POST',
      url: 'sendmail.php',
      data: data,
      success: function() {
        $('.modal').popup('hide');
        $('#thanks').popup('show');
        setTimeout(function() {
          $(formName).trigger('reset');
        }, 2000);
      }
    });
  };
});
